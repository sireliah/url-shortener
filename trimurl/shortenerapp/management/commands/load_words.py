
import re
import unicodedata
from django.core.management.base import BaseCommand

from shortenerapp.models import Word


class Command(BaseCommand):

    help = 'Load list of words for url shortener.'

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str)

    def handle(self, *args, **options):
        new_count = 0
        for line in self.get_lines(options['filename']):
            parsed = self.parse_line(line)
            new_count = self.create_if_new(parsed, new_count)

        self.stdout.write(self.style.SUCCESS('Added %s new words.' % new_count))

    @staticmethod
    def create_if_new(parsed, new_count):
        """
        Add new word if it wasn't in the db yet.
        """
        if parsed:
            word_exists = Word.objects.filter(text=parsed).exists()
            if not word_exists:
                Word.objects.create(text=parsed)
                new_count += 1
        return new_count

    def get_lines(self, filename):
        """
        Yields one line at time instead loading file into memory.
        """
        try:
            with open(filename, 'r') as _file:
                for line in _file:
                    yield line
        except FileNotFoundError as e:
            self.stdout.write(self.style.ERROR(e))

    def parse_line(self, line):
        try:
            # Normalize unicode characters to ASCII.
            line = unicodedata.normalize('NFKD', line).encode('ascii', 'ignore')

            # Decode from bytes to unicode and strip non alpha-numeric chars.
            line = re.sub('[^0-9a-z]+', '', line.decode('utf-8').lower())
            return line
        except TypeError as e:
            self.stdout.write(self.style.ERROR(e))
        except ValueError as e:
            self.stdout.write(self.style.ERROR(e))
        return None
