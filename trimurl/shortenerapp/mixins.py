
import re
from urllib.parse import urlparse


class WordMixin(object):

    @staticmethod
    def extract_words_from_original_url(url):
        """
        Try to split url path into words.
        Alternatively the domain name can be used.
        """
        word_list = []
        parsed = urlparse(url)
        path_list = re.split(r'/|-|_', parsed.path)
        query_list = re.split(r'=|&', parsed.query)
        domain_list = parsed.netloc.split('.')[:-1]

        word_list = path_list + query_list + domain_list

        return [word for word in word_list if word]

    @staticmethod
    def get_word(extracted_words, word_model, urlmap_model):
        """
        This method fetches the matching word from db.
        1) one of the exactly matching words
        2) not matching, but a free word
        3) if still nothing found, the oldest, already used one
        """
        word = None
        words_found = word_model.objects.filter(
            text__in=extracted_words,
            urlmap__isnull=True,
        ).order_by('pk')

        if words_found.exists():
            word = words_found.first()
        else:
            word = word_model.objects.filter(
                urlmap__isnull=True
            ).order_by('pk').last()

        if not word:
            # Free the oldest word.
            oldest_url = urlmap_model.objects.all().order_by('pk').first()
            if oldest_url:
                word = oldest_url.word
                oldest_url.delete()
            else:
                # TODO: what should we do when there are no words?
                pass

        return word