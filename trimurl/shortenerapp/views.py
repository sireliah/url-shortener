
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic.edit import FormView

from shortenerapp.forms import ShortUrlForm
from shortenerapp.models import UrlMap


class ShortUrlView(FormView):

    """
    View for GET and POST.
    """

    form_class = ShortUrlForm
    template_name = 'form.html'
    success_url = '/'

    def form_valid(self, form):
        original_url = "%s" % self.request.POST.get('original')

        urlmap = UrlMap(original=original_url)
        urlmap.save()
        new_url = self.request.build_absolute_uri(urlmap.shortened)
        new_url_html = '<a href="%s">%s</a>' % (new_url, new_url)
        messages.success(self.request, 'Your new url: %s' % new_url_html,
                         extra_tags='safe')
        return render(self.request, 'form.html',
                      {'form': form, 'new_url': urlmap.shortened})


class RedirectView(View):

    def get(self, request, *args, **kwargs):
        word = "%s" % kwargs.get('word')
        urlmap = get_object_or_404(UrlMap, word__text=word)

        return HttpResponseRedirect(urlmap.original)
