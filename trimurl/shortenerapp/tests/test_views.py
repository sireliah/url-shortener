
from django.test import TestCase, Client

from shortenerapp.models import Word


class TestUrlViews(TestCase):

    def setUp(self):
        self.client = Client()

    def test_index_page(self):
        response = self.client.get('/')

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'form.html')
        self.assertContains(response, 'Shorten your url!')

    def test_index_post_invalid(self):
        response = self.client.post('/', {'original': 'wrong'})

        # Aparently form_invalid returns 200 instead of 400.
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'form.html')
        self.assertContains(response, 'Enter a valid URL.')

    def test_index_post_valid(self):
        Word.objects.create(text='word')
        response = self.client.post('/', {'original': 'http://correct.com'})

        # Aparently form_invalid returns 200 instead of 400.
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'form.html')
        self.assertContains(response, 'Your new url:')

    def test_index_post_valid_existingword(self):
        Word.objects.create(text='existingword')
        response = self.client.post('/', {'original': 'http://correct.com/existingword/'})

        # Aparently form_invalid returns 200 instead of 400.
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'form.html')
        self.assertContains(response, 'existingword')

    def test_index_post_valid_firstfromdb(self):
        Word.objects.create(text='firstfromdb')
        response = self.client.post('/', {'original': 'http://correct.com/notexistingword/'})

        # Aparently form_invalid returns 200 instead of 400.
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'form.html')
        self.assertContains(response, 'firstfromdb')