
from django.test import TestCase

from shortenerapp.forms import ShortUrlForm


class TestForm(TestCase):

    def test_form_valid(self):
        form = ShortUrlForm(data={'original': 'http://correct.com'})
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = ShortUrlForm(data={'original': 'incorrect'})
        self.assertFalse(form.is_valid())
        expected = '<ul class="errorlist"><li>Enter a valid URL.</li></ul>'
        self.assertEqual(expected, str(form.errors['original']),)

    def test_form_incorrect_arg(self):
        form = ShortUrlForm(data={'wrong': ''})
        self.assertFalse(form.is_valid())
        expected = '<ul class="errorlist"><li>This field is required.</li></ul>'
        self.assertEqual(expected, str(form.errors['original']),)

    def test_form_empty(self):
        form = ShortUrlForm(data={'original': ''})
        self.assertFalse(form.is_valid())
        expected = '<ul class="errorlist"><li>This field is required.</li></ul>'
        self.assertEqual(expected, str(form.errors['original']),)
