
from django.test import TestCase

from shortenerapp.mixins import WordMixin
from shortenerapp.models import Word, UrlMap


class TestWordMixin(TestCase):

    """
    extract_words_from_original_url
    """

    def test_extract_words_from_original_url_one_word(self):
        url = 'http://correct.com/existingword/'
        result = WordMixin.extract_words_from_original_url(url)
        self.assertEqual(['existingword', 'correct'], result)

    def test_extract_words_from_original_url_two_words(self):
        url = 'http://correct.com/first/second/'
        result = WordMixin.extract_words_from_original_url(url)
        self.assertListEqual(['first', 'second', 'correct'], result)

    def test_extract_words_from_original_url_different_separators(self):
        url = 'http://correct.com/first-second/floor__more'
        result = WordMixin.extract_words_from_original_url(url)
        self.assertListEqual(
            ['first', 'second', 'floor', 'more', 'correct'], result
        )

    def test_extract_words_from_original_url_params(self):
        url = 'http://correct.com/word?param1=value1&param2=value2'
        result = WordMixin.extract_words_from_original_url(url)
        self.assertListEqual(
            ['word', 'param1', 'value1', 'param2', 'value2', 'correct'], result
        )

    def test_extract_words_from_original_url_empty(self):
        url = ''
        result = WordMixin.extract_words_from_original_url(url)
        self.assertListEqual([], result)

    """
    get_word
    """
    def test_get_word_exists(self):
        existing = Word.objects.create(text='existing')
        result = WordMixin.get_word(['existing', ], Word, UrlMap)
        self.assertEqual(existing, result)

    def test_get_word_last_one(self):
        Word.objects.create(text='first')
        last = Word.objects.create(text='last')

        result = WordMixin.get_word(['something', ], Word, UrlMap)
        self.assertEqual(last, result)

    def test_get_word_delete_oldest(self):
        first = Word.objects.create(text='first')
        UrlMap.objects.create(original='a', shortened='b', word=first)

        result = WordMixin.get_word(['something', ], Word, UrlMap)

        self.assertEqual(first, result)

        maps = UrlMap.objects.filter(original='a')
        self.assertEqual(0, maps.count())