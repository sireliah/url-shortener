
from io import StringIO

from django.core import management
from django.core.management.base import CommandError
from django.test import TestCase

from shortenerapp.management.commands.load_words import Command
from shortenerapp.models import Word


class TestLoadWords(TestCase):

    """
    integration
    """
    def test_load_words_one(self):
        out = StringIO()
        management.call_command('load_words', 'shortenerapp/tests/files/words.txt', stdout=out)

        words = Word.objects.filter(text='word')
        self.assertTrue('Added 1 new words.' in out.getvalue())
        self.assertEqual(1, words.count())

    def test_load_words_no_argument(self):
        out = StringIO()
        with self.assertRaises(CommandError):
            management.call_command('load_words', stdout=out)

    def test_load_words_argument_no_file(self):
        out = StringIO()
        management.call_command('load_words', 'non_existing.txt', stdout=out)
        self.assertTrue('No such file' in out.getvalue())

    """
    get_lines
    """
    def test_get_lines_correct(self):
        command = Command()
        line = command.get_lines('shortenerapp/tests/files/words.txt')

        self.assertEqual('word', next(line))
        with self.assertRaises(StopIteration):
            self.assertEqual('', next(line))

    def test_get_lines_empty(self):
        command = Command()
        line = command.get_lines('shortenerapp/tests/files/words_empty.txt')

        with self.assertRaises(StopIteration):
            self.assertEqual('', next(line))

    """
    parse_line
    """
    def test_parse_line_correct(self):
        command = Command()
        result = command.parse_line('correctword')
        self.assertEqual('correctword', result)

    def test_parse_line_unicode_diacritic(self):
        command = Command()
        result = command.parse_line('ąąążżżźźćććć')
        self.assertEqual('aaazzzzzcccc', result)

    def test_parse_line_more_unicode(self):
        command = Command()
        result = command.parse_line('going now ᕕ( ᐛ )ᕗ')
        self.assertEqual('goingnow', result)

    def test_parse_line_non_alphanumeric(self):
        command = Command()
        result = command.parse_line('aaa__()___^^^^')
        self.assertEqual('aaa', result)

    def test_parse_line_non_uppercase(self):
        command = Command()
        result = command.parse_line('UPPER')
        self.assertEqual('upper', result)

    """
    create_if_new
    """
    def test_create_if_new_one(self):
        count = 0
        count = Command.create_if_new('new_one', count)
        words = Word.objects.filter(text='new_one')

        self.assertEqual(1, count)
        self.assertEqual(1, words.count())

    def test_create_if_new_duplicated(self):
        count = 0
        count = Command.create_if_new('new_one', count)
        count = Command.create_if_new('new_one', count)

        words = Word.objects.filter(text='new_one')
        self.assertEqual(1, count)
        self.assertEqual(1, words.count())

    def test_create_if_new_blank(self):
        count = 0
        count = Command.create_if_new('', count)

        words = Word.objects.filter(text='')
        self.assertEqual(0, count)
        self.assertEqual(0, words.count())