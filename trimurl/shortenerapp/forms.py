
from django import forms

from shortenerapp.models import UrlMap


class ShortUrlForm(forms.ModelForm):

    original = forms.CharField(label='Your URL')

    class Meta:
        fields = ('original', )
        model = UrlMap
