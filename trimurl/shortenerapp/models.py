
from django.db import models
from django.urls import reverse

from shortenerapp.mixins import WordMixin


class Word(models.Model):

    """
    Model for word, that will be used to construct shortened urls.
    """

    # unique=True also creates an index.
    text = models.CharField(max_length=255, unique=True)

    class Meta:
        db_table = 'word_list'

    def __str__(self):
        return '%s' % self.text


class UrlMap(models.Model, WordMixin):

    """
    Here we store pair od old and new url.
    Also we keep relation to word that was used in the new url.
    """

    original = models.URLField(max_length=1024)
    shortened = models.URLField()
    word = models.OneToOneField(Word, on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'url_map'
        unique_together = ('original', 'shortened', 'word', )

    def save(self, *args, **kwargs):
        extracted_words = self.extract_words_from_original_url(self.original)

        self.word = self.get_word(extracted_words, Word, UrlMap)
        self.shortened = reverse('redirect_view', kwargs={'word': self.word})

        super().save(*args, **kwargs)
