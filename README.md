# Url-Shortener app #
Piotr Gołąb

2017

### Prepare the project: ###
cd url-shortener/

virtualenv-3.5 venv

source venv/bin/activate

pip3 install -r requirements.txt

### Run tests: ###
cd trimurl/

python3 manage.py test

### Migrate db: ###
python3 manage.py migrate

### Load words: ###
python3 manage.py load_words example/words.txt

### Run server and try the app: ###
python3 manage.py runserver

http://127.0.0.1:8000/

### Notes: ###